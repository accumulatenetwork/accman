![](AccMan.png)

Video Walkthrough: https://www.youtube.com/watch?v=r8H6UEEMq-o

# AccMan - The Accumulate Manager runner

This is the recommended method of running a node on the Accumulate network.

#### Prerequisites

- A Linux server that matches or exceeds the minimum specification for intended purpose.
- Ubuntu is recommended (other Debian based distributions should work out of the box). Any distro with Docker support should work.

#### Install

Log in as root.

```
apt update && apt -y install git telnet net-tools
git clone https://gitlab.com/accumulatenetwork/accman.git
cd accman
./scripts/install-ubuntu.sh <-- this will install the latest verion of Docker.
```

Note: The firewall assumes ssh access is on port 22. Read the firewall section if this is not the case. Failing to do so, will lock you out of your server. If you have an external firewall, you will need to open ports for Accumulate to operate (see below).

#### Run & Configure

All operations are done, using the run command.

```
./accman
```

If you want to create a ssh login account that takes you directly to the Accumulate Manager menu:

```
./accman createuser accman
```

This will create a login account 'accman'. The private keys from ~/.ssh/authorized_keys will be copied to /home/accman/.ssh/authorized_keys. Edit as necessary.

---

#### Firewall & Rate Limiting

Locking down your server is important. By default Accumulate Manager will take care of this for you*. To make manual changes, edit the `iptables.sh` file accordingly.

*It is still your responsibility to make sure your server is secure.

**Ports in use:**
|Port|Use|
|--|--|
|16591|DN P2P|
|16592|DN RPC|
|16595|DN RPC Json|
|16691|BVN P2P|
|16692|BVN RPC|
|16695|BVN RPC Json|
|16666|AccMan|

|Proxy Port|Use|
|--|--|
|6695|SSL Client (your-domain.com)|


#### SSL certificates

A self-certificate is automatically generated on start-up.
If you have an existing certificate for a domain name, copy the `.crt` and `.key` files to the `./certs` directory. They will be read on start-up. `.pem` files are also compatible. Rename them to `.crt` and `.key` files respectively. Set up a cron job, to make sure the files are kept valid.  These files will be monitored for changes, and automatically reloaded.
If you provide an e-mail address, Accumulate Manager will attempt to get a certificate from LetsEncrypt, and keep it renewed.
