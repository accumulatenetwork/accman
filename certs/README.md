Add your two SSL certificate files here. 

```
domain.crt
domain.key
```

The file name is not important, but must end in `.crt` and `.key`
Accumulate-Manager will monitor these files for changes, and re-load them.
Set up a cron job to renew the certificates periodically.
.pem files are compatible.  Rename the file extensions accordingly.

This will override automated LetsEncrypt.