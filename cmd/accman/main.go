package main

import (
	"context"
	"os"
	"os/signal"
	"strings"

	"github.com/AccumulateNetwork/ssh_config"
	"github.com/spf13/cobra"
)

func main() {
	_ = cmd.Execute()
}

var cmd = &cobra.Command{
	Use:   "accman [server]",
	Short: "Connect to accman",
	Args:  cobra.ExactArgs(1),
	Run:   run,

	ValidArgsFunction: complete,
}

func init() {
	// This is a hack to ensure the help and completion subcommands are
	// generated. Normally they wouldn't be since accman doesn't have any
	// subcommands.
	dummy := new(cobra.Command)
	cmd.AddCommand(dummy)
	cmd.InitDefaultHelpCmd()
	cmd.InitDefaultCompletionCmd()
	cmd.RemoveCommand(dummy)
}

func run(_ *cobra.Command, args []string) {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	// Setup an SSH connection
	ssh, err := SSH(args[0])
	check(err)

	// Connect to the backend
	telnet(ctx, ssh)
}

func complete(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	var results []string
	ssh_config.ForEach(func(host *ssh_config.Host) bool {
		if len(host.Patterns) != 1 || !host.Patterns[0].Simple() {
			return true
		}
		if strings.HasPrefix(host.Patterns[0].String(), toComplete) {
			results = append(results, host.Patterns[0].String())
		}
		return true
	})
	return results, cobra.ShellCompDirectiveDefault
}
