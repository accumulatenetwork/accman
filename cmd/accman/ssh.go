package main

import (
	"bufio"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net"
	"net/url"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"github.com/AccumulateNetwork/ssh_config"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"golang.org/x/crypto/ssh/knownhosts"
)

// SSH opens an SSH connection. SSH expects a URL in the format
// 'ssh[-scheme]://[user@[:identity]]alias[:port][/path]'.
//
// SSH will use ~/.ssh/config to resolve the alias and any unspecified
// settings. The user defaults to the current user and the port defaults to 22,
// if they are not specified directly and are not specified in ~/.ssh/config.
func SSH(s string) (*ssh.Client, error) {
	if !strings.Contains(s, "://") {
		s = "ssh://" + s
	}
	u, err := url.Parse(s)
	if err != nil {
		return nil, err
	}

	alias := u.Hostname()
	hostName := ssh_config.Get(alias, "HostName")
	if hostName == "" {
		hostName = alias
	}

	port := ssh_config.Get(alias, "Port")
	if port == "" {
		port = "22"
	}

	cu, err := user.Current()
	if err != nil {
		return nil, fmt.Errorf("resolve current user: %w", err)
	}

	config := new(ssh.ClientConfig)
	config.User = u.User.Username()
	if config.User == "" {
		config.User = ssh_config.Get(alias, "User")
		if config.User == "" {
			config.User = cu.Username
		}
	}

	var files []string
	for _, file := range strings.Split(ssh_config.Get(alias, "UserKnownHostsFile"), " ") {
		if strings.HasPrefix(file, "~/") {
			file = filepath.Join(cu.HomeDir, file[2:])
		}
		_, err := os.Stat(file)
		switch {
		case err == nil:
			files = append(files, file)
		case !errors.Is(err, fs.ErrNotExist):
			return nil, err
		}
	}
	config.HostKeyCallback, err = hostKeyCallbackForFiles(files)
	if err != nil {
		return nil, fmt.Errorf("load known hosts: %w", err)
	}

	identityAgent := os.Getenv("SSH_AUTH_SOCK")
	if identityAgent == "" {
		identityAgent = ssh_config.Get(alias, "IdentityAgent")
	}
	if identityAgent != "" {
		if strings.HasPrefix(identityAgent, "~/") {
			identityAgent = filepath.Join(cu.HomeDir, identityAgent[2:])
		}
		sock, err := net.Dial("unix", identityAgent)
		if err != nil {
			return nil, fmt.Errorf("connect to identity agent: %w", err)
		}
		defer sock.Close() // Is it ok to close this here?
		config.Auth = append(config.Auth, ssh.PublicKeysCallback(agent.NewClient(sock).Signers))
	}

	identityFile, ok := u.User.Password()
	if !ok {
		identityFile = ssh_config.Get(alias, "IdentityFile")
	}
	if identityFile != "" {
		config.Auth = append(config.Auth, ssh.PublicKeysCallback(func() ([]ssh.Signer, error) {
			b, err := os.ReadFile(identityFile)
			switch {
			case err == nil:
				// Ok
			case errors.Is(err, fs.ErrNotExist):
				return nil, nil
			default:
				return nil, err
			}
			signer, err := ssh.ParsePrivateKey(b)
			if err != nil {
				return nil, err
			}
			return []ssh.Signer{signer}, nil
		}))
	}

	return ssh.Dial("tcp", hostName+":"+port, config)
}

func hostKeyCallbackForFiles(files []string) (ssh.HostKeyCallback, error) {
	stdin := bufio.NewReader(os.Stdin)
	kh, err := knownhosts.New(files...)
	if err != nil {
		return nil, fmt.Errorf("load known hosts: %w", err)
	}

	return func(hostname string, remote net.Addr, key ssh.PublicKey) error {
		err := kh(hostname, remote, key)
		if err == nil {
			return nil
		}
		var kerr *knownhosts.KeyError
		if !errors.As(err, &kerr) && len(kerr.Want) < 1 {
			return err
		}

		if i := strings.IndexByte(hostname, ':'); i >= 0 {
			hostname = hostname[:i]
		}

		fmt.Printf(unknownHostPrompt[1:]+" ", hostname, remote, key.Type(), ssh.FingerprintSHA256(key))
	answer:
		for {
			answer, err := stdin.ReadString('\n')
			if err != nil {
				return err
			}
			switch strings.TrimSpace(answer) {
			case "yes":
				break answer
			case "no":
				return kerr
			}
			fmt.Print(badYesPrompt[1:] + " ")
		}

		keyB64 := base64.StdEncoding.EncodeToString(key.Marshal())
		line := fmt.Sprintf("%s %s %s\n", hostname, key.Type(), keyB64)

		f, err := os.OpenFile(kerr.Want[0].Filename, os.O_RDWR, 0600)
		if err != nil {
			return err
		}
		_, err = f.Seek(-1, io.SeekEnd)
		if err != nil {
			return err
		}
		var b [1]byte
		_, err = f.Read(b[:])
		if err != nil {
			return err
		}
		if b[0] != '\n' {
			line = "\n" + line
		}

		_, err = f.Write([]byte(line))
		if err != nil {
			return err
		}

		err = f.Close()
		if err != nil {
			return err
		}

		kh, err = knownhosts.New(files...)
		if err != nil {
			return err
		}

		return nil
	}, nil
}

const unknownHostPrompt = `
The authenticity of host '%s (%s)' can't be established.
%s key fingerprint is %s.
Are you sure you want to continue connecting (yes/no)?`

const badYesPrompt = `
Invalid answer. Are you sure you want to continue connecting (yes/no)?`
