package main

import (
	"context"
	"fmt"
	"io"
	"os"

	"golang.org/x/crypto/ssh"
	"golang.org/x/term"
)

func telnet(ctx context.Context, ssh *ssh.Client) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Connect to the backend
	conn, err := ssh.Dial("tcp", "localhost:3333")
	check(err)
	go func() { <-ctx.Done(); _ = conn.Close() }()

	// Put the terminal in raw mode
	old, err := term.MakeRaw(int(os.Stdin.Fd()))
	check(err)
	go func() { <-ctx.Done(); check(term.Restore(int(os.Stdin.Fd()), old)) }()

	// Pipe IO
	go copy(conn, os.Stdin, cancel)
	go copy(os.Stdout, conn, cancel)

	<-ctx.Done()
}

func copy(dst io.Writer, src io.Reader, cancel context.CancelFunc) {
	_, err := io.Copy(dst, src)
	cancel()
	if err != nil {
		fmt.Println("Error:", err)
	}
}
