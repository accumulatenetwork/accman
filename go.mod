module gitlab.com/accumulatenetwork/accman

go 1.22.1

require (
	github.com/AccumulateNetwork/ssh_config v0.0.0-20240424222350-293fbee61b3f
	github.com/spf13/cobra v1.8.0
)

require golang.org/x/sys v0.19.0 // indirect

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/crypto v0.22.0
	golang.org/x/term v0.19.0
)
