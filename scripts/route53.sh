#!/bin/sh
# For use with Amazon Route53

export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
DOMAIN=mydomain.com

install()
{
  snap install core
  snap refresh core
  snap install --classic certbot
  ln -s /snap/bin/certbot /usr/bin/certbot
  snap set certbot trust-plugin-with-root=ok
  snap install certbot-dns-route53  
}

command -v certbot &> /dev/null || install


function find_and_copy_most_recent_file() {
    search_path="$1"
    search_filename="$2"
    destination_file="$3"

    most_recent_file=$(find "$search_path" -xtype f -name "$search_filename" -printf "%T@ %p\n" | sort -nr | head -n1 | awk '{print $2}')

    if [ -n "$most_recent_file" ]; then
        if [ ! -e "$destination_file" ] || [ "$most_recent_file" -nt "$destination_file" ]; then
            cp "$most_recent_file" "$destination_file"
            echo "$most_recent_file File copied successfully."
        else
            echo "The destination file $destination_file is already up-to-date."
        fi
    else
        echo "No files matching the search filename were found."
    fi
}



certbot certonly \
  --webroot-path $PWD \
  --dns-route53 \
  --non-interactive --agree-tos \
  -d $DOMAIN
  
find_and_copy_most_recent_file "/etc/letsencrypt/live/" "fullchain.pem" "domain.crt"
find_and_copy_most_recent_file "/etc/letsencrypt/live/" "privkey.pem" "domain.key"
